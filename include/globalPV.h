
typedef struct var_global
{
  portMUX_TYPE timerMux;
  uint32_t interruptCounter;
  uint32_t totalInterruptCounter;
} var_global;

extern var_global data_global;

extern SemaphoreHandle_t timerSemaphore1;
extern SemaphoreHandle_t timerSemaphore2;

#define LED1 18
#define LED2 19

#define TOUCH1 4
#define TOUCH2 2