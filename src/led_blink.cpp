#include <Arduino.h>
#include "globalPV.h"
#include "globalPF.h"

void TaskLed(void *pvParameters)
{
    //
    const int freq = 5000;
    const int ledChannel = 0;
    const int resolution = 5;
    const int maxZnach = 1 << (resolution);

    ledcSetup(ledChannel, freq, resolution); //настройка канала LEDC
    ledcAttachPin(LED2, ledChannel);
    //    pinMode(LED1, OUTPUT);

    while (1)
    {
        // vTaskDelay(1);
        static int bright;
        xSemaphoreTake(timerSemaphore2, portMAX_DELAY);
        if (touchRead(TOUCH1) < 20 && touchRead(TOUCH1) < 20 && touchRead(TOUCH1) < 20 && bright < maxZnach)
        // 3 раза ситывание как защита от ложного срабатывани (и даже работает:) )
        {
            bright++;
        }

        if (touchRead(TOUCH2) < 20 && touchRead(TOUCH2) < 20 && touchRead(TOUCH2) < 20 && bright >= 1)
        {
            bright--;
        }
        Serial.printf("bright: %d  maxZnach: %d \n", bright, maxZnach);
        ledcWrite(ledChannel, bright);

        //       digitalWrite(LED1, !digitalRead(LED1));
    }
}