#include <EEPROM.h>
#include <Arduino.h>
#include "globalPV.h"
#include "globalPF.h"

// определите количество байтов, к которым вы хотите получить доступ
#define EEPROM_SIZE 4

void TaskEEPROM(void *pvParameters)
{
    Serial.begin(115200);
    // инициализировать EEPROM с предопределенным размером

    // EEPROM.begin(EEPROM_SIZE);
    //  прочитать последнее состояние счетчика из флэш-памяти
    // EEPROM.write(0, 0);
    // EEPROM.commit();

    data_global.interruptCounter = EEPROM.read(0);

    while (1)
    {
        for (size_t i = 0; i < 10; i++)
        {
            /* code */
            xSemaphoreTake(timerSemaphore1, portMAX_DELAY);
            data_global.interruptCounter++;
            printf("counter: %d \n", data_global.interruptCounter);
        }

        printf("write to EEPROM: %d \n", data_global.interruptCounter);
        // EEPROM.write(0, data_global.interruptCounter);
        // EEPROM.commit();
    }
}