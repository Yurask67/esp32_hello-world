#include <Arduino.h>
#include "globalPV.h"
#include "globalPF.h"

const int timeSeconds = 10; // количество секунд, в течение которых светодиод будет оставаться включенным после обнаружения движения.
// задаем GPIO-контакты для светодиода и PIR-датчика движения:
const int motionSensor = 27;
// вспомогательные переменные для таймера:
/* проверяем, обнаружено ли движение,
 и если обнаружено, переключаем контакт «LED1» на «HIGH»
 и запускаем таймер:*/
void IRAM_ATTR detectsMovement()
{
    Serial.println("MOTION DETECTED!!!");
    //  "ОБНАРУЖЕНО ДВИЖЕНИЕ!!!"
    digitalWrite(LED1, HIGH);
}

void vTaskGpioISR(void *pvParameters)
{
    // запускаем последовательную коммуникацию (в целях отладки):
    Serial.begin(115200);
    // выставляем контакт датчика движения в режим «INPUT_PULLUP»:
    pinMode(motionSensor, INPUT_PULLUP);
    /* делаем контакт «motionSensor» контактом для прерываний,
     задаем функцию, которая будет запущена при прерывании,
     а также выставляем режим активации прерывания на «RISING»:*/
    attachInterrupt(digitalPinToInterrupt(motionSensor), detectsMovement, FALLING);
    // переключаем контакт светодиода («LED1») на «LOW»:
    pinMode(LED1, OUTPUT);
    digitalWrite(LED1, LOW);

    while (1)
    {
        // выключаем светодиод спустя определенное количество секунд,
        // заданное в параметре «timeSeconds»:
        if (LED1)
        {
            vTaskDelay(timeSeconds * 1000 / portTICK_PERIOD_MS);
            Serial.println("Motion stopped...");
            //  "Движение прекратилось..."
            digitalWrite(LED1, LOW);
        }
    }
}