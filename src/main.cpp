#include <Arduino.h>
#include "globalPV.h"
#include "globalPF.h"

SemaphoreHandle_t timerSemaphore1;
SemaphoreHandle_t timerSemaphore2;

void setup(void)
{
  data_global.interruptCounter = 0;

  Serial.begin(115200);
  timerSemaphore1 = xSemaphoreCreateBinary();
  timerSemaphore2 = xSemaphoreCreateBinary();

  xTaskCreate(task_timer, "task_timer", 2048, NULL, 5, NULL);
  xTaskCreate(TaskLed, "TaskLed", 2048, NULL, 5, NULL);
  xTaskCreate(TaskEEPROM, "TaskEEPROM", 2048, NULL, 5, NULL);
  xTaskCreate(vTaskGpioISR, "TaskEEPROM", 2048, NULL, 5, NULL);
}
void loop(void)
{
}