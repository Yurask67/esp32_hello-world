#include <Arduino.h>
#include "globalPV.h"
#include "globalPF.h"

hw_timer_t *timer = NULL;

var_global data_global = {
    .timerMux = portMUX_INITIALIZER_UNLOCKED};

void IRAM_ATTR onTimer() // колбек прерывания по таймеру

{
    static uint16_t CountIsrTimer1; //счетчик прерываний 1
    static uint16_t CountIsrTimer2; //счетчик прерываний 2
    portENTER_CRITICAL_ISR(&data_global.timerMux);
    CountIsrTimer1++;
    CountIsrTimer2++;

    if (CountIsrTimer1 >= 1000)
    {
        CountIsrTimer1 = 0;
        xSemaphoreGiveFromISR(timerSemaphore1, 0);
    }

    if (CountIsrTimer2 >= 300)
    {
        CountIsrTimer2 = 0;
        xSemaphoreGiveFromISR(timerSemaphore2, 0);
    }
    portEXIT_CRITICAL_ISR(&data_global.timerMux);
}

void task_timer(void *pvParameters)
{

    //   static uint16_t CountIsrTimer1; //счетчик прерываний 1
    //   static uint16_t CountIsrTimer2; //счетчик прерываний 2

    Serial.begin(115200);
    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &onTimer, true);
    timerAlarmWrite(timer, 1000, true); //через 1 мс прерывание
    timerAlarmEnable(timer);

    while (1)
    {
        vTaskDelay(1);

        xSemaphoreTake(timerSemaphore1, portMAX_DELAY);
        Serial.print("An interrupt as occurred. Total number: \n");
    }
}